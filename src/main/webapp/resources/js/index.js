/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
function Modal(elements) {
    this.elements = elements;
}

Modal.prototype.init = function() {
    this.bindEvents();
}

Modal.prototype.bindEvents = function() {
    this.elements.signup_submit.on("click", this.checkSignupFields.bind(this));
//    this.checkSignupFields();
}

Modal.prototype.checkSignupFields = function(ev) {
    ev.preventDefault();
//    console.log("in check");
    if(this.elements.signup_password.val() != this.elements.signup_confirm_password.val()) {
        alert("Password does not match.");
        return;
    }
//    if(this.elements.signup_identification_number.val().length != 9 || !$.isNumeric(this.elements.signup_identification_number)) {
//        alert("Identification Number must be of 9 digits.");
//        return;
//    }
//    if(this.elements.signup_phone.val().length != 10 || !$.isNumeric(this.elements.signup_phone)) {
//        alert("Phone Number must be of 10 digits.");
//        return;
//    }
//    
//    console.log(this);
    this.elements.signup_form.submit();
}

$(() => {
//    alert("hey");
//    console.log("hey");
    let elements = {
        login_email: $("#defaultForm-email"),
        login_password: $("#defaultForm-pass"),
        signup_email: $("#orangeForm-email"),
        signup_password: $("#orangeForm-pass"),
        signup_confirm_password: $("#orangeForm-confpass"),
        signup_phone: $("#orangeForm-phone"),
        signup_roles: $(".signup_role_radio"),
        signup_name: $("#orangeForm-name"),
        signup_identification_number: $("#orangeForm-idnum"),
        signup_submit: $("#signup_submit"),
        signup_form: $("#signup_form")
    };
    
    let m = new Modal(elements);
    m.init();
})