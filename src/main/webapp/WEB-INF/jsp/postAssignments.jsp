<%-- 
    Document   : postAssignments
    Created on : Mar 30, 2020, 2:03:51 AM
    Author     : manish
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Slateboard</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.14.1/js/mdb.min.js"></script>
        <link href="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.14.1/css/mdb.min.css" rel="stylesheet">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css">
        <link rel="icon" type="image/png" sizes="32x32" href="<c:url value="/resources/favicon/favicon-32x32.png" />">
        <script src="<c:url value="/resources/js/index.js" />"></script>
    </head>

    <body>

        <!--login modal-->
        <div class="modal fade" id="modalLoginForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
             aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header text-center">
                        <h4 class="modal-title w-100 font-weight-bold">Sign in</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body mx-3">
                        <div class="md-form mb-5">
                            <i class="fas fa-envelope prefix grey-text"></i>
                            <input type="email" id="defaultForm-email" class="form-control validate">
                            <label data-error="wrong" data-success="right" for="defaultForm-email">Your email</label>
                        </div>

                        <div class="md-form mb-4">
                            <i class="fas fa-lock prefix grey-text"></i>
                            <input type="password" id="defaultForm-pass" class="form-control validate">
                            <label data-error="wrong" data-success="right" for="defaultForm-pass">Your password</label>
                        </div>
                        <div class="form-group mb-4">
                            <label>What's your role?</label> &ensp;
                            <label class="radio-inline"><input type="radio" class="signup_role_radio" name="su_role" value="professor" required>Professor</label>
                            <label class="radio-inline"><input type="radio" class="signup_role_radio" name="su_role" value="teaching_assistant">Teaching Assistant</label>
                            <label class="radio-inline"><input type="radio" class="signup_role_radio" name="su_role" value="student">Student</label>
                        </div>
                    </div>
                    <div class="modal-footer d-flex justify-content-center">
                        <button class="btn btn-default">Login</button>
                    </div>
                </div>
            </div>
        </div>

        <!--Sign up Modal-->
        <div class="modal fade" id="modalRegisterForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
             aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header text-center">
                        <h4 class="modal-title w-100 font-weight-bold">Sign up</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <form id="signup_form" action="signup_auth.htm" method="post">
                        <div class="modal-body mx-3">
                            <div class="md-form mb-5">
                                <i class="fas fa-user prefix grey-text"></i>
                                <input type="text" name="su_name" id="orangeForm-name" class="form-control validate" required>
                                <label data-error="wrong" data-success="right" for="orangeForm-name">Your name</label>
                            </div>
                            <div class="md-form mb-5">
                                <i class="fas fa-envelope prefix grey-text"></i>
                                <input type="email" name="su_email" id="orangeForm-email" class="form-control validate" required>
                                <label data-error="wrong" data-success="right" for="orangeForm-email">Your email</label>
                            </div>

                            <div class="md-form mb-4">
                                <i class="fas fa-lock prefix grey-text"></i>
                                <input type="password" name="su_password" id="orangeForm-pass" class="form-control validate" required>
                                <label data-error="wrong" data-success="right" for="orangeForm-pass">Your password</label>
                            </div>

                            <div class="md-form mb-4">
                                <i class="fas fa-check-double prefix grey-text"></i>
                                <input type="password" name="su_confirm_password" id="orangeForm-confpass" class="form-control validate" required>
                                <label data-error="wrong" data-success="right" for="orangeForm-confpass">Confirm password</label>
                            </div>
                            <div class="md-form mb-5">
                                <i class="fas fa-phone prefix grey-text"></i>
                                <input type="text" minlength="10" maxlength="10" name="su_phone" id="orangeForm-phone" class="form-control validate" required>
                                <label data-error="wrong" data-success="right" for="orangeForm-phone">Your Phone Number</label>
                            </div>
                            <div class="form-group mb-4">
                                <label>What's your role?</label> &ensp;
                                <label class="radio-inline"><input type="radio" class="signup_role_radio" name="su_role" value="professor" required>Professor</label>
                                <label class="radio-inline"><input type="radio" class="signup_role_radio" name="su_role" value="teaching_assistant">Teaching Assistant</label>
                                <label class="radio-inline"><input type="radio" class="signup_role_radio" name="su_role" value="student">Student</label>
                            </div>
                            <div class="md-form mb-5">
                                <i class="fas fa-id-card prefix grey-text"></i>
                                <input type="text" minlength="9" maxlength="9" name="su_identification_number" id="orangeForm-idnum" class="form-control validate" required>
                                <label data-error="wrong" data-success="right" for="orangeForm-idnum">Your Identification Number</label>
                            </div>
                        </div>
                        <div class="modal-footer d-flex justify-content-center">
                            <button type="submit" name="su_submit" id="signup_submit" class="btn btn-deep-orange">Sign up</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>



        <div class="jumbotron text-center" style="margin-bottom: 0;">

            <h1>Slateboard</h1>
            <p style="padding-left: 20%;">~ A Virtual Classroom.</p> 

        </div>
    <nav class="navbar navbar-inverse">
        <div class="container-fluid">

            <ul class="nav navbar-nav">
                <li><a href="#">Home</a></li>
                <li><a href="#">Course Material</a></li>
                <li><a href="#">Assignments</a></li>
                <li><a href="#">Courses</a></li>
                <li><a href="#">Grading</a></li>
                <li><a href="#">Post Assignments</a></li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <li><a href="#" data-toggle="modal" data-target="#modalRegisterForm"><span class="glyphicon glyphicon-user mb-4"></span> Sign Up</a></li>
                <li><a href="#" data-toggle="modal" data-target="#modalLoginForm"><span class="glyphicon glyphicon-log-in mb-4"></span> Login</a></li>
            </ul>
        </div>
    </nav>
    <div class="container">
        <div class="row">
            <div class="col-sm-4">
                <h3>Column 1</h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit...</p>
                <p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris...</p>
            </div>
            <div class="col-sm-4">
                <h3>Column 2</h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit...</p>
                <p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris...</p>
            </div>
            <div class="col-sm-4">
                <h3>Column 3</h3>        
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit...</p>
                <p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris...</p>
            </div>
        </div>
    </div>
</body>
</html>
