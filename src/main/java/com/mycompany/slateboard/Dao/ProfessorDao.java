package com.mycompany.slateboard.Dao;

import com.mycompany.slateboard.pojo.Professor;
import java.util.ArrayList;
import java.util.List;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author manish
 */
public class ProfessorDao {
    private static final SessionFactory sf = new Configuration().configure("hibernate.cfg.xml").buildSessionFactory();
    private Session session = null;

    private Session getSession() {
        if (session == null || !session.isOpen()) {
            session = sf.openSession();
        }
        return session;
    }

    private void beginTransaction() {
        getSession().beginTransaction();
    }

    private void commit() {
        getSession().getTransaction().commit();;
    }

    private void close() {
        getSession().close();
    }

    private void rollbackTransaction() {
        getSession().getTransaction().rollback();
    }
    
    public boolean checkProfessorExistance(String professorIdentificationNumber) {
        List<Professor> professors = new ArrayList<Professor>();
        try {
            beginTransaction();
            Query q = getSession().createQuery("from Professor where professor_identification_number = :pid");
            q.setString("pid", professorIdentificationNumber);
            professors = q.list();
            commit();
        } catch(HibernateException e) {
            e.printStackTrace();
            rollbackTransaction();
        } finally {
            close();
        }
        
        return professors.size() >= 1;
    }

    public int addProfessor(String name, String email, String password, String phone, String professorIdentificationNumber) {
        int result = 0;
        try {
            Professor professor = new Professor();
            professor.setName(name);
            professor.setEmail(email);
            professor.setPassword(password);
            professor.setPhone(phone);
            professor.setProfessorIdentificationNumber(professorIdentificationNumber);
            beginTransaction();
            getSession().save(professor);
            commit();
            result = 1;
        } catch (HibernateException e) {
            e.printStackTrace();
            rollbackTransaction();
        } finally {
            close();
        }
        return result;
    }
    
    
//    public void deleteMessageById(long id){
//        try {
//            beginTransaction();
//            Query q = getSession().createQuery("from Message where id= :id");
//            q.setLong("id", id);
//            Message msgToDelete = (Message)q.uniqueResult();
//            getSession().delete(msgToDelete);
//            commit();
//        } catch (HibernateException e) {
//            e.printStackTrace();
//            rollbackTransaction();
//        } finally {
//            close();
//        }
//    }

    public int getProfessorId(String email, String password) {
        List<Professor> professors = new ArrayList<Professor>();
        int result = 0;
        try {
            beginTransaction();
            Query q = getSession().createQuery("select id from Professor where email = :email and password = :password");
            q.setString("email", email);
            q.setString("password", password);
            professors = q.list();
            commit();
            result = professors.size();
        } catch(HibernateException e) {
            e.printStackTrace();
            rollbackTransaction();
        } finally {
            close();
        }
        
        return result;
    }
}
