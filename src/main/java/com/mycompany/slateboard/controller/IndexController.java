/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.slateboard.controller;

import com.mycompany.slateboard.Dao.ProfessorDao;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.AbstractController;

/**
 *
 * @author manish
 */
@WebServlet(name = "IndexController", urlPatterns = {"/IndexController"})
public class IndexController extends AbstractController {

    public IndexController() {
    }

    @Override
    protected ModelAndView handleRequestInternal(
            HttpServletRequest request,
            HttpServletResponse response) throws Exception {
        ModelAndView mv = null;
        String su_role = request.getParameter("su_role");
        String login_role = request.getParameter("login_role");
//        String confirmPassword = request.getParameter("su_confirm_password");
        if (su_role != null) {
            if (su_role.equals("professor")) {
                ProfessorDao professorDao = new ProfessorDao();
                String name = request.getParameter("su_name");
                String email = request.getParameter("su_email");
                String password = request.getParameter("su_password");
                String identificationNumber = request.getParameter("su_identification_number");
                String phone = request.getParameter("su_phone");
                boolean exist = professorDao.checkProfessorExistance(identificationNumber);
                if (!exist) {
                    int result = professorDao.addProfessor(name, email, password, phone, identificationNumber);
                    if (result == 1) {
                        request.setAttribute("message", "professor signup success!");
                        mv = new ModelAndView("view");
                    } else {
                        request.setAttribute("message", "professor signup failed; result=" + result);
                        mv = new ModelAndView("view");
                    }
                    response.getWriter().print(result);
                } else {
                    request.setAttribute("message", "Your credentials already exist, Please login to continue");
                    mv = new ModelAndView("view");
                }
            }
        } else if (login_role != null) {
            if (login_role.equals("professor")) {
                ProfessorDao professorDao = new ProfessorDao();
                String email = request.getParameter("login_email");
                String password = request.getParameter("login_password");
                int id = professorDao.getProfessorId(email, password);
                if (id != 0) {
                    request.setAttribute("message", "login success, id = " + id);
                } else {
                    request.setAttribute("message", "login fail");
                }
            }
            mv = new ModelAndView("view");
        } else {
            mv = new ModelAndView("index");
        }
        return mv;
    }

}
