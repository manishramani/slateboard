/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mycompany.slateboard.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author manish
 */
@Controller
public class TryController {
  
    	@RequestMapping(value = "/new.htm", method = RequestMethod.GET)
	public String handleRequest(HttpServletRequest request, HttpServletResponse response) {
		request.setAttribute("message", "Neww");
		return "view";
	}
}
