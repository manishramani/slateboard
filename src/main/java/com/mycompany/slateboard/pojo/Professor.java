/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.slateboard.pojo;

/**
 *
 * @author manish
 */
public class Professor {
    int id;
    String name;
    String email;
    String password;
    String phone;
    String professorIdentificationNumber;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return "Professor{" + "id=" + id + ", name=" + name + ", email=" + email + ", password=" + password + ", phone=" + phone + ", professorIdentificationNumber=" + professorIdentificationNumber + '}';
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getProfessorIdentificationNumber() {
        return professorIdentificationNumber;
    }

    public void setProfessorIdentificationNumber(String professorIdentificationNumber) {
        this.professorIdentificationNumber = professorIdentificationNumber;
    }
    
}
